# Interview

I'd like to give a bit more information about how I do technical screening and interviews. The main reason for me to doing is to give me and the candidate more change to be aligned what expectations and requirements are.

The most important thing for us is hand-on experience so please **bring your own laptop** which you are able to use for:
* connecting to servers using SSH (`tmate`)
* share screen if necessary

We do all screenings on shared session same way as we do team/pair debugging. Some problems are simulated on the cluster and we kindly ask you to fix it. It is up to you if you want to describe the actions while working/coding or afterwards. If you don't understand the question please don't hesitate to ask. 

## DevOps 
You will connect to `tmate` session do fixing some broken server. It can be some linux problem, docker question or Kubernetes issue. We don't expect you to remember all the commands and parameter so if you can't remember then feel free to check documentation or ask.

## Coding
I'm expecting you to do coding using your laptop because it should be the most comfortable way for you. You are free to use any editor you want and I'm only asking you to share your progress with me by sharing your screen.
